const express = require('express')
const app = express()

app.set('PORT', process.env.PORT || 3000)

app.get('/', (req, res) => {
    res.status(200).send({
        message: 'Hello World!!!!!!!!, A message from 117404792',
    })
})

app.listen(app.get('PORT'), () =>
    console.log(`Server running on port ${app.get('PORT')}`),
)
